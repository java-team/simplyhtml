#!/bin/sh

_debug() {
	if [ -n "${DEBUG}" ]
	then
		echo "DEBUG:   $1" >&2
		shift
		for text in "$@"
		do
			echo "         ${text}" >&2
		done
	fi
}

_error() {
	echo "ERROR:   $1" >&2
	shift
	for text in "$@"
	do
		echo "         ${text}" >&2
	done
}

findjava() {
	# We try hard to find the proper 'java' command
	if [ -n "${JAVACMD}" ] && [ -x "${JAVACMD}" ]
	then
		_debug "Using \$JAVACMD to find java virtual machine."
	elif [ -n "${JAVA_BINDIR}" ] && [ -x "${JAVA_BINDIR}/java" ]
	then
		JAVACMD="${JAVA_BINDIR}/java"
		_debug "Using \$JAVA_BINDIR to find java virtual machine."
	elif [ -n "${JAVA_HOME}" ] && [ -x "${JAVA_HOME}/bin/java" ]
	then
		JAVACMD="${JAVA_HOME}/bin/java"
		_debug "Using \$JAVA_HOME to find java virtual machine."
	else
		JAVACMD=$(which java)
		if [ -n "${JAVACMD}" ] && [ -x "${JAVACMD}" ]
		then
			_debug "Using \$PATH to find java virtual machine."
		elif [ -x /usr/bin/java ]
		then
			_debug "Using /usr/bin/java to find java virtual machine."
			JAVACMD=/usr/bin/java
		fi
	fi

	# if we were successful, we return 0 else we complain and return 1
	if [ -n "${JAVACMD}" ] && [ -x "${JAVACMD}" ]
	then
		_debug "Using '$JAVACMD' as java virtual machine..."
		if [ -n "${DEBUG}" ]
		then
			"$JAVACMD" -version >&2
		fi
		if (! "${JAVACMD}" -version 2>&1 | grep -qe 'Java(TM)' -e OpenJDK)
		then
			_error "Your Java virtual machine is not certified," \
			       "=======================================" \
			       "FREEMIND WILL MOST PROBABLY *NOT* WORK," \
			       "=======================================" \
			       "define JAVACMD, JAVA_BINDIR, JAVA_HOME or PATH in order" \
			       "to point to such a VM. See the manpage of freemind(1) for details."
			return 0
		fi
		return 0
	else
		_error "Couldn't find a java virtual machine," \
		       "define JAVACMD, JAVA_BINDIR, JAVA_HOME or PATH." \
		       "See the manpage of freemind(1) for details."
		return 1
	fi
}

_source() {
	if [ -f "$1" ]
	then
		_debug "Sourcing '$1'."
		. "$1"
	fi
}

output_info() {
	if [ -z "${DEBUG}" ]
	then
		return 0
	fi
	_debug "Freemind parameters are '${@}'."
	_debug "$(uname -a)"
	if [ -x $(which dpkg) ]
	then
		_debug "The following DEB packages are installed:"
		COLUMNS=132 dpkg -l \*simplyhtml\* \*j\* | grep -v '<none>' >&2
	elif [ -x $(which rpm) ]
	then
		_debug "The following RPM packages are installed:"
		rpm -qa | grep -i -e simplyhtml -e j >&2
	else
		_debug "Neither dpkg nor rpm is installed."
	fi
}

findjava
if [ $? -ne 0 ]
then
	exit 1
fi

output_info

# The CLASSPATH also lets one specify additional jars, which is good, if
# you want to add a new Look&Feel jar (the motif one is so ugly...).
#
#CLASSPATH="${ADD_JARS}:${CLASSPATH}:\
#/usr/share/java/gnu-regexp.jar:\
#/usr/share/java/jhall.jar:\
#/usr/share/java/SimplyHTMLHelp.jar:\
#/usr/share/java/SimplyHTML.jar:"

_debug "Calling: '${JAVACMD} -jar /usr/share/java/SimplyHTML.jar com.lightdev.app.shtm.App $@'."
"${JAVACMD}" -jar /usr/share/java/SimplyHTML.jar com.lightdev.app.shtm.App "$@"
